# audio

Sounds, songs, beeps, breedles, warbles, algoraves, live code, sonic hacks

To start, this will collect all my random bits-and-pieces from [snippets in hax](https://gitlab.com/milohax/hax/-/snippets) and GitHub gists.
