#Knight Rider Main Theme
# by Stu Phillips, 1981
# Sonic Pi transcript, Mike Lockhart, 2018
# https://gitlab.com/milohax/hax/-/snippets/2231645
# https://gist.github.com/sinewalker/8a05322b9c3c7ab976c4b246bb0e806f

# (missing Rtyhm Section: Buffer Capacity reached)

#motifs

define :kr0 do |n|
  play_pattern_timed [n, :r, n+1, n,  n, n+1, n, n], 0.25
end

define :kr1 do |n|
  play_pattern_timed [n+1, n, n, n,  n-1, n, n, n], 0.25
end

define :kb0 do |n|
  2.times do
    play_pattern_timed [n, n, :r, :r], 0.25
  end
end

define :kb1 do |n|
  play_pattern_timed [n, n, :r, :r], 0.25
  sleep 1
end

define :kb2 do |n|
  play_pattern_timed [n, n, :r, :r], 0.25
  play_pattern_timed [n+3, n+3, :r, :r], 0.25
end

define :kitt do |n, x, a|
  in_thread do
    x.times do
      play_pattern_timed [n, n+1, n, n-2], 0.25, amp: a, sustain: 0.2
    end
  end
end

################################

use_debug false

use_bpm 118
with_fx :reverb, room: 0.7, amp: 1.1, damp: 0.2 do
  
  
  in_thread do                                         ########### MAIN
    use_synth :pluck
    use_synth_defaults sustain: 0.25, release: 0.1, amp: 1.5
    
    #This drives the song and plays the guitar plucks which run through the whole piece
    #Deliberately not using loops here: need to cue the other parts and count bars
    
    kr0 :Cs4; kr1 :Cs4; kr0 :Cs4; kr1 :Cs4
    kr0 :B3; kr1 :B3; kr0 :B3; kr1 :B3
    
    cue :drums_part #also bass rises
    kr0 :Cs4; kr1 :Cs4;  kr0 :Cs4; kr1 :Cs4
    kr0 :B3; kr1 :B3;  kr0 :B3;
    cue :guitar
    
    kr1 :B3
    
    
    cue :chords_part, out: false
    kr0 :Cs4; kr1 :Cs4;  kr0 :Cs4; kr1 :Cs4
    
    kr0 :B3; kr1 :B3; kr0 :B3;  cue :claps; kr1 :B3
    
    cue :theme, sust: 3
    kr0 :Cs4; kr1 :Cs4; kr0 :Cs4; kr1 :Cs4; kr0 :Cs4
    kr0 :B3; kr1 :B3; kr0 :B3;
    
    
    kr0 :Cs4; kr1 :Cs4; kr0 :Cs4; kr1 :Cs4; kr0 :Cs4
    kr0 :B3; kr1 :B3; kr0 :B3; kr1 :B3; play_pattern_timed [:B3, :r, :r, :r], 0.25
    cue :tada
    
    sleep 1
    
    cue :gliss
    kr0 :Cs4; kr1 :Cs4; kr0 :Cs4; kr1 :Cs4
    kr0 :B3; kr1 :B3; kr0 :B3; kr1 :B3
    
    cue :theme, sust: 1
    kr0 :Cs4; kr1 :Cs4; kr0 :Cs4;
    kr0 :B3; kr1 :B3; kr0 :B3;
    
    kr0 :Cs4; kr1 :Cs4; kr0 :Cs4;
    kr0 :B3; kr1 :B3; kr0 :B3;
    cue :tada
    
    cue :chords_part, out: true
    kr0 :Cs4; kr1 :Cs4; kr0 :Cs4; kr1 :Cs4
    
    kr0 :B3; kr1 :B3;
    
    cue :outro
    kr0 :Cs4; kr1 :Cs4; kr0 :Cs4; play_pattern_timed [:D4, :Cs4, :C4, :Cs4], 0.25
    play :Cs4
  end
  
  in_thread do                                         ########### bass
    
    use_synth :subpulse
    
    use_transpose -12
    use_synth_defaults amp: 6, attack: 0, release: 0.6,
      cutoff: 70, pulse_width: 0.9
    
    kb0 :Fs3; 3.times do kb1 :Fs3; end
    kb0 :E3; 3.times do kb1 :E3; end
    
    kb0 :Fs3; 3.times do kb1 :Fs3; end
    kb0 :G3; 3.times do kb1 :G3; end
    
    kb0 :Fs3; 3.times do kb1 :Fs3; end
    
    
    kb0 :E3; 3.times do kb1 :E3; end
    
    4.times do kb1 :Fs3; end; kb0 :Fs3;
    2.times do kb1 :E3; end; kb2 :E3;
    
    4.times do kb1 :Fs3; end; kb0 :Fs3;
    2.times do kb1 :E3; end; 2.times do kb0 :E3; end; play_pattern_timed [:E3], 0.25
    
    sleep 1.25
    
    play :Cs3; sleep 0.5
    kb0 :Fs3; 3.times do kb1 :Fs3; end
    kb0 :E3; 3.times do kb1 :E3; end
    
    kb0 :Fs3; 2.times do kb1 :Fs3; end
    kb0 :E3; kb1 :E3; kb0 :E3;
    
    kb0 :Fs3; 2.times do kb1 :Fs3; end
    kb0 :E3; kb1 :E3; play_pattern_timed [:E3, :E3], 0.25
    sleep 1; play :Cs3; sleep 0.5
    
    kb0 :Fs3; 3.times do kb1 :Fs3; end
    
    kb0 :E3; kb1 :E3;
    
    2.times do kb1 :Fs2; end;
    2.times do
      play_pattern_timed [:Fs2, :Fs2, :r, :r], 0.25
    end
    
    sleep 0.5
    play_pattern_timed [:Cs3, :Fs3], 0.5
  end
  
  live_loop :do_chords do                              ########### chords
    use_synth :dsaw
    use_synth_defaults cutoff: 130,
      detune: 0.11, env_curve: 1, release: 1.5
    
    values = sync :chords_part
    with_fx :flanger, phase: 8, depth: 12 do
      sleep 1
      s = play_chord chord(:G4, :major, invert: 1), sustain: 6.25
      sleep 0.25
      control s, notes: (chord :Fs4, :major)
      sleep 7.75
      
      s = play_chord chord(:Fs4, :major), sustain: 6.25
      sleep 0.25
      control s, notes: (chord :E4, :minor, invert: 1)
      if values[:out]
        sleep 0.5
        control s, amp: 0, amp_slide: 4.5
        stop
      end
    end
  end
  
  in_thread do                                         ########### gliss
    use_synth :dsaw
    with_fx :ixi_techno, phase: 8, cutoff_min: 100 do
      use_synth_defaults cutoff: 130,
        detune: 0.04, env_curve: 1, attack: 0.5, release: 2.5, amp: 0.5
      
      sync :gliss
      
      s = play :Fs4,
        sustain: 9, decay: 4, note_slide: 7.5, pan: -1, pan_slide: 12
      sleep 0.5
      control s, note: :B5, pan: 1
    end
  end
  
  
  live_loop :do_theme do                              ########### theme
    
    use_synth :dsaw
    use_synth_defaults cutoff: 100,
      detune: 0.05, env_curve: 1, amp: 1.2
    
    values = sync :theme
    
    sust = values[:sust]
    
    play_chord chord(:Fs4, :major), sustain: 10, amp: 0.4
    play_pattern_timed [:Fs4, :G4, :Fs4, :Cs5],
      [0.5,0.25,0.25,sust]
    play_pattern_timed [:Fs5, :G5, :Fs5, :Cs5],
      [0.5,0.25,0.25,sust]
    play_pattern_timed [:Fs4, :G4, :Fs4, :Cs5, :Fs5],
      [0.5,0.25,0.25,0.5,0.5]
    play_chord chord(:E4, :minor), sustain: 6, amp: 0.4
    play :E5, sustain: 5
    
    sleep 1
    use_synth_defaults cutoff: 100,
      detune: 0.05, env_curve: 2, amp: 0.6, decay: 0.06
    
    [0.1, 0.2, 0.3, 0.4, 0.5].each do |a|
      kitt :Fs4, 1, a+0.2
      sleep 1
    end
    use_synth_defaults cutoff: 100,
      detune: 0.05, env_curve: 1, amp: 1.2
    play_chord chord(:Fs4, :major), sustain: 10, amp: 0.4
    play_pattern_timed [:Fs4, :G4, :Fs4, :Cs5],
      [0.5,0.25,0.25,sust]
    play_pattern_timed [:Fs5, :G5, :Fs5, :Cs5],
      [0.5,0.25,0.25,sust]
    play_pattern_timed [:Fs4, :G4, :Fs4, :Cs5, :Fs5],
      [0.5,0.25,0.25,0.5,0.5]
    if sust > 1
      play_chord chord(:E4, :minor), sustain: 8, amp: 0.4
      play :G5, sustain: 8
      sleep 1
      use_synth_defaults cutoff: 100,
        detune: 0.05, env_curve: 2, amp: 0.6, decay: 0.06
      kitt :Fs4, 7, 0.75
      sleep 2
      kitt :B4, 5, 0.75
      sleep 2
      kitt :E5, 3, 0.75
      sleep 3
      play_chord [:Fs4, :B4, :E5], sustain: 0, amp: 4, attack: 0, decay: 0.3
      
      use_synth_defaults cutoff: 100,
        detune: 0.05, env_curve: 1, amp: 0.6
      sleep 1.5
      play_chord chord(:E4, :minor), sustain: 0, amp: 4, attack: 0
      sleep 0.5
      play_chord chord(:Fs4, :major), sustain: 0, amp: 4, attack: 0
    else
      play_chord chord(:E4, :minor), sustain: 4, amp: 0.3
      play :G5, sustain: 4
      sleep 1
      use_synth_defaults cutoff: 100,
        detune: 0.05, env_curve: 2, amp: 0.6, decay: 0.06
      kitt :Fs4, 4, 0.75
      sleep 1
      kitt :B4, 3, 0.75
      sleep 1
      kitt :E5, 2, 0.75
      sleep 2
      use_synth_defaults cutoff: 100,
        detune: 0.05, env_curve: 1, amp: 0.6
      sleep 0.5
      play_chord chord(:E4, :minor), sustain: 0, amp: 4, attack: 0
      sleep 0.5
      play_chord chord(:Fs4, :major), sustain: 0, amp: 4, attack: 0
      stop
    end
    
  end
  
  in_thread do                                         ########### outro
    use_synth :dsaw
    
    sync :outro
    
    use_synth_defaults cutoff: 130,
      detune: 0.015, env_curve: 1, attack: 0.5, release: 2.5, amp: 0.48
    s = play :Cs4,
      sustain: 6, note_slide: 3, pan: -1, pan_slide: 6
    control s, note: :Cs5, pan: 1
    
    use_synth :fm
    t = play :Cs5,
      sustain: 6, note_slide: 4, pan: 1, pan_slide: 6, amp: 0.48
    control t, note: :Fs3, pan: -1
    
    use_synth :dsaw
    use_synth_defaults cutoff: 100,
      detune: 0.05, env_curve: 1, amp: 1.6
    
    play_pattern_timed [:Cs4, :D4, :Cs4],
      [1.25,0.25,0.5]
    play_pattern_timed [:Fs4, :G4, :Fs4],
      [1.25,0.25,0.5]
    control s, note: :Cs6, pan: 0
    play_pattern_timed [:Cs5], 2.25
    play_pattern_timed [:D5, :Cs5, :Fs5],
      [0.25,0.5,0.5]
  end
end

