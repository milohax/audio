# 20220420 12:26 Synth case overload
# https://in-thread.sonic-pi.net/t/different-tracks-created-with-sonic-pi-last-month/6698

live_loop :met do
    sleep 1
  end
  
  with_fx :reverb, room: 0.8 do
    
    live_loop :mel1, sync: :met do
      use_synth :saw
      #use_synth :tri
      c = range(70,130,steps: 2.5).shuffle.tick(:range)
      p = knit(0+12,7, 3+12,2).tick
      riff1 = rrand_i(1,4)
      case riff1
      when 1
        play :f1, release: 0.15, pan: rrand(-1,1), cutoff: c, pitch: p
        #     sleep 0.125
      when 2
        play :f2, release: 0.15, pan: rrand(-1,1), cutoff: c, pitch: p
      when 3
        play :c3, release: 0.15, pan: rrand(-1,1), cutoff: c, pitch: p
        use_synth :tri
        play :c5, release: 0.08, pan: rrand(-1,1), pitch: ring(0,5,12,-5).choose#, cutoff: c
      when 4
        sleep 0.125
        sleep 0.125
      end
      sleep 0.125
    end
    
    live_loop :mel2, sync: :met do
      #stop
      use_synth :dsaw
      #use_synth :beep
      c = range(70,130,steps: 2.5).shuffle.tick(:range)
      p = knit(0,7, 3,2).tick
      riff2 = rrand_i(1,2)
      case riff2
      when 1
        play :f1, release: 0.15, pan: rrand(-1,1), cutoff: c, pitch: p
        #     sleep 0.125
      when 2
        play :f2, release: 0.15, pan: rrand(-1,1), cutoff: c, pitch: p
      when 3
        #      use_synth :tri
        #play :c5, release: 0.04, pan: rrand(-1,1), pitch: ring(0,5,12,-5).choose#, cutoff: c
      when 4
        sleep 0.125
        sleep 0.125
        
      end
      sleep 0.125
    end
    
    #oneshot
    use_synth :gnoise
    # play :f1, attack: 8, release: 8
    
  end #end reverb
  
  with_fx :bitcrusher, bit: 1.2 do
    live_loop :kick, sync: :met do
      sample :bd_haus, cutoff: 80, amp: 4, finish: 0.7 if spread(3,7).tick(:kick)
      #sleep 0.25
      sample :sn_generic, cutoff: 110, amp: 0.5, finish: 0.05,  pan: -0.5 if spread(5,7).tick(:kick)
      sample :sn_generic, cutoff: 110, amp: 0.7, finish: 0.03, pan: 0.5 if spread(3,5).tick(:kick)
      sleep 0.25
    end
  end