https://gist.github.com/sinewalker/b7019b2e8ecb4b1a0de79f8c9a7d4b28

Boulder Dash for ATARI 2600, by Peter Liepa, 1984

A Live Loop to algorave to.

Original music (Youtube, Commodore 64 version): https://www.youtube.com/watch?v=14CAO72_pJw
A pretty accurate transcription:  https://musescore.com/user/3769276/scores/1076731  (Two mistakes: should be `:Ab4` instead of `:A4` on the second part at position 15, and the first part, at position 47).

Interesting note:  Boulder Dash was originally written for the ATARI 2600 video game console, which had a TIA sound chip
with two oscillators to generate tones (max polyphony was 2). So on both the Youtube recording (which is of a C64 port)
and the MIDI score transcription, only two notes are ever played at once.

More on Boulder Dash: https://en.wikipedia.org/wiki/Boulder_Dash

8 bars, 16 semiquavers per bar, two-tone polyphony, consequently no actual rests, but the bass and treble parts bounce
between the two occilators. 128 x 2 notes. Repeats forever.

---

Play with toggling the `stop`s on each part while it plays, or turning on and changing the `fx`. Or you can hear it in a new key with `transpose`, and change the speed with `use_bpm` or the values of `dur`.
