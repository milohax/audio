# https://gitlab.com/milohax/hax/-/snippets/2231648

aud0 =       [ :F3,  :A3,  :C4,  :F4,    :G3, :Bb3,  :C4,  :G4,   :Db4, :Eb4,  :F4, :Ab4,   :Eb4,  :D5,  :E4,  :C5]
aud0.concat  [ :F3,  :F4,  :C3,  :G3,   :Eb3,  :G4,  :G3, :Eb3,    :F3,  :F4,  :C3,  :G3,   :Db3,  :F5,  :F3, :Db3]
aud0.concat  [:Eb3, :Eb4, :Bb3,  :F3,    :B3, :Eb5, :Eb4,  :B3,    :C2,  :E4,  :C2,  :F4,   :Bb3, :Bb3, :Bb4, :Bb3]
aud0.concat  [ :F4,  :F4,  :F4,  :F4,    :F4,  :F4,  :F4,  :F4,    :F4,  :F4,  :F4,  :F4,    :F4,  :F4,  :F4,  :F4]
aud0.concat  [ :F4,  :A4,  :F4, :Bb4,    :F4,  :A4,  :F4, :Bb4,    :F4,  :A4,  :F4, :Bb4,   :Eb4,  :G4, :Eb4, :Ab4]
aud0.concat  [ :F4,  :F5,  :F4, :Eb5,    :F4,  :D5,  :F4, :Ab4,   :Eb4, :Eb5, :Eb4, :Eb5,   :Eb4, :Bb4, :Eb4, :Eb5]
aud0.concat  [ :F4,  :A4,  :F4, :Bb4,    :F4,  :A4,  :F4, :Bb4,    :F4,  :A4,  :F4, :Bb4,   :Eb4,  :G4, :Eb4, :Ab4]
aud0.concat  [ :A4,  :F4,  :C4,  :A3,    :G4, :Eb4, :Bb3, :Eb3,    :C5,  :A4,  :F4,  :C4,    :G4, :Eb4, :Bb3, :Eb3]

aud0=aud0.ring

aud1 =       [ :F2,  :C3,  :F3, :Ab3,   :Eb2,  :D3, :Eb3, :Bb3,   :Db2, :Db2, :Db3, :Db2,   :Eb3, :Bb4,  :E3, :Ab4]
aud1.concat  [ :F2,  :F2,  :F2,  :F2,   :Eb2, :Eb2, :Eb2, :Eb2,    :F2,  :F2,  :F2,  :F2,   :Db3, :Db3, :Db3, :Db3]
aud1.concat  [:Eb2, :Eb2, :Eb2, :Eb2,    :B2,  :B2,  :B2,  :B2,    :C2,  :C3,  :C2,  :C3,   :Bb2, :Bb2,  :F2,  :F2]
aud1.concat  [ :F2,  :F2,  :F2,  :F2,    :F3,  :F3,  :F2,  :F2,   :Eb2, :Eb2, :Eb2, :Eb2,   :Eb3, :Eb3, :Eb2, :Eb2]
aud1.concat  [ :F2,  :F4,  :F2,  :F4,    :F3,  :F4,  :F2,  :F4,   :Eb2,  :F4, :Eb2,  :F4,   :Eb3, :Eb4, :Eb2, :Eb4]
aud1.concat  [ :F2,  :F2,  :F2,  :C5,    :F3,  :F3,  :F2,  :C5,   :Eb2, :Eb2, :Eb2, :Eb2,   :Eb3, :Eb3, :Eb2, :Eb2]
aud1.concat  [ :F2,  :F4,  :F2,  :F4,    :F3,  :F4,  :F2,  :F4,   :Eb2,  :F4, :Eb2,  :F4,   :Eb3, :Eb4, :Eb2, :Eb4]
aud1.concat  [ :F4,  :C4,  :A3,  :F3,   :Eb4, :Bb3,  :G3, :Eb2,    :A4,  :F4,  :C4,  :A3,   :Bb3,  :G3, :Eb3, :Eb2]

aud1=aud1.ring

####

use_bpm 93
tick_reset
set :position, 0

dur=0.25
phz=4

use_transpose 0
use_synth :tri
use_synth_defaults attack: 0, decay: 0, sustain: dur, release: 0.2

live_loop :metro do
  sleep dur
  set :position, tick
end

live_loop :au0 do
  ##| stop
  use_synth :tri
  ##| with_fx :ixi_techno, phase: phz, res: 0.95, cutoff_max: 129, cutoff_min: 50 do #, delay: 8, feedback: 5
  ##| steps=phz/dur
  ##| steps.times do
  play aud0[sync :position], pan: -0.5
  ##| end
  ##| end
end

live_loop :au1 do
  ##| stop
  use_synth :subpulse
  ##| with_fx :bitcrusher do
  ##| :position.nil? ?
  play aud1[sync :position], pan: 0.5#, sustain: 0.0125#, sustain: 0.0125, pan: 1, amp: 2
  ##| end
end

