use_bpm 150
use_synth :tech_saws
live_loop :crash do
  2.times do
    play_pattern_timed [:C4,:r,:C4,:F4, :G4,:C5,:r,:Bb4], 0.5
    play_pattern_timed [:r,:G4,:r,:G4,  :Gb4,:G4,:Gb4,:G4], 0.5
  end
  
  play_pattern_timed [:F4,:F4, :r], 1
  play_pattern_timed [:r,:F4], 0.5
  play_pattern_timed [:G4,:G4,:G4], 0.5
  sleep 2.5
  play_pattern_timed [:F4,:F4], 1
  play_pattern_timed [:r,:F4, :G4, :F4], 0.5
end

live_loop :drums do
  2.times do
    sample :bd_haus
    wait 0.5
  end
  sample :sn_dub
  wait 0.5
  sample :drum_cymbal_soft
  wait 0.5
end
