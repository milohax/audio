# https://gist.github.com/darinwilson/6d38f7697aa52a006b03cc2df5acf31c

# Source code for a Sonic Pi piece (http://sonic-pi.net/)
#
# This an extension of an earlier piece I wrote with
# Sonic Pi called "Ambient Experiment". I added a new 
# voice and routed the notes out via MIDI so that the
# sounds could be generated in Ableton Live. 
#
# Audio version: https://soundcloud.com/darinwilson/cpu-dreams
#
# The piece consists of four long loops, each of which
# plays one of a few randomly selected pitches. The sustain
# values are also random, so the notes move in and 
# out of phase with each other. This can play for quite 
# awhile without repeating itself :)

live_loop :note1 do
  midi choose([:Cs3,:D4,:E4]), sustain: choose([5,7,9]), channel: 1
  sleep 8
end
  
live_loop :note2 do
  midi choose([:Fs4,:G4,:A4]), sustain: choose([5,7,9]), channel: 1
  sleep 10
end
  
live_loop :note3 do
  midi choose([:B4,:Cs5,:E5]), sustain: choose([5,7,9]), channel: 1
  sleep 11
end
  
live_loop :note4 do
  midi choose([:D2, :A2, :Fs2, :G2]), sustain: choose([7,8,10]), channel: 4
  sleep 14
end

