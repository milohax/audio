use_synth :dsaw
use_bpm 125
sweet_dreams_bass_right = [:C2,  :C3,  :C4,  :C5,  :Eb5, :Eb5, :C5,  :C5,
                           :Ab2, :Ab3, :Ab4, :C5,  :G2,  :G3,  :G4,  :C5]
sweet_dreams_bass_left  = [:C2,  :r,   :C3,  :C2,  :r,   :C3,  :C2,  :C3,
                           :Ab2, :r,   :Ab3, :Ab2, :G2,  :G3,  :Bb2, :Bb3]


live_loop :bass do
  ##| wait 8; stop
  ##| sync "/live_loop/bass_left"
  play_pattern_timed sweet_dreams_bass_right, 0.5, amp: 0.5, pan: 0.8, pitch: -12, release: 0.2
end

live_loop :bass_left, sync: "/live_loop/bass" do
  ##| stop
  with_fx :flanger, phase: 8, depth: 6, wave: 3, feedback: 0.4, delay: 2 , invert_flange: 1 do
    sweet_dreams_bass_left.each do |n|
      play n,  amp: 0.5, pan: -0.8, pitch: 0.20, release: 0.3 unless n == :r
      wait 0.5
    end
  end
end

live_loop :drums, sync: "/live_loop/bass_left" do
  ##| stop
  sample :bd_boom, amp: 3.5
  sample :bd_tek, amp: 2
  sample :bd_fat, amp: 2
  wait 1
  7.times do
    sample :bd_fat, amp: 2
    wait 1
  end
end

########

live_loop :sweet_dreams, sync: "/live_loop/bass" do
  stop
  use_synth :dpulse
  use_synth_defaults release: 2.4
  
  wait 1
  
  #sweet dreams are made of this
  play_pattern_timed [:Eb4, :Eb4, :C4, :Eb4, :Eb4, :Eb4, :D4],
    [1, 1, 1, 0.5, 1, 0.5, 1.5]
  
  #who am I to disagree?
  sync "/live_loop/bass"
  play_pattern_timed [:Eb4, :Eb4, :C4, :Eb4, :C4, :Eb4, :F4, :Eb4, :D4],
    [0.5, 0.5, 0.5, 1.5, 1, 0.5, 1, 0.5, 1.5]
  
  #I travel the world and the seven seas
  play_pattern_timed [:C4, :Eb4, :Eb4, :C4, :Eb4, :Eb4, :C4, :Eb4, :Eb4, :Eb4, :D4],
    [0.5, 0.5, 0.5, 0.5, 1.5, 0.5, 0.5, 0.5, 1, 0.5, 1.5]
  #everybody's looking for something
  sync "/live_loop/bass"
  play_pattern_timed [:Eb4, :C4, :Eb4, :C4, :Eb4, :Eb4, :F4, :Eb4, :D4],
    [1, 0.5, 1, 1.5, 0.5, 0.5, 0.5, 0.5, 1.5]
  sync "/live_loop/bass"
end

########

define :harmonise do |partA, partB, playTime, harmony|
  partA.each_with_index do |a, i|
    play a
    play partB[i]+harmony
    wait playTime[i]
  end
end


live_loop :some_of_them, sync: "/live_loop/bass" do
  stop
  use_synth :dpulse
  use_synth_defaults release: 1.8
  harmony = 4
  
  #some of them want to use you
  A = [:Eb4, :Eb4, :C4, :Eb4, :C4, :Eb4, :C4]
  B = [:Eb4, :Eb4, :B3, :Eb4, :B3, :Eb4, :Db4]
  T = [0.5, 0.5, 1, 1, 0.5, 1, 1.5]
  harmonise A, B, T, harmony
  
  #some of them want to be used by you
  A = [:Eb4, :Eb4, :C4, :Eb4, :Eb4, :C4, :Eb4, :F4, :Eb4, :D4]
  B = [:Eb4, :Eb4, :B3, :Eb4, :Eb4, :B3, :Eb4, :E4, :Eb4, :Db4]
  T = [0.5, 0.5, 1, 0.5, 0.5, 0.5, 1, 1.5, 0.5, 1]
  sync "/live_loop/bass"
  harmonise A, B, T, harmony
  
  #some of them want to abuse you
  A = [:Eb4, :Eb4, :C4, :Eb4, :Eb4, :C4, :Eb4, :C4]
  B = [:Eb4, :Eb4, :B3, :Eb4, :Eb4, :B3, :Eb4, :Db4]
  T = [0.5, 0.5, 1, 0.5, 0.5, 0.5, 1, 1]
  sync "/live_loop/bass"
  harmonise A, B, T, harmony
  
  #some of them want to be abused
  A = [:Eb4, :Eb4, :C4, :Eb4, :C4, :Eb4, :F4, :Eb4, :D4]
  B = [:Eb4, :Eb4, :B3, :Eb4, :B3, :Eb4, :E4, :Eb4, :Db4]
  T = [0.5, 0.5, 1, 1, 0.5, 1, 1.5, 0.5, 1]
  sync "/live_loop/bass"
  harmonise A, B, T, harmony
  
  sync "/live_loop/bass"
end
