# This Boys In Love - The Presets

use_synth :saw
use_bpm 120


BASS = 1
CHORDS = 1
ARPS = 1
VERSE = 1

BOYSINLOVE = 0
BRIDGE = 0

def arps_on
  return ARPS == 1
end

def bass_on
  return BASS == 1
end

def chords_on
  return CHORDS == 1
end

def verse_on
  return VERSE == 1
end

def boys_in_love
  return BOYSINLOVE == 1
end

def bridge
  return BRIDGE == 1
end

with_fx :reverb, room: 0.7 do
  live_loop :kick do
    ##| stop
    sample :bd_haus, amp: 2
    sleep 0.5
    sample :hat_cab
    sleep 0.5
    sample :sn_generic, amp: 0.5
    sample :bd_haus, amp: 2
    sleep 0.5
    sample :hat_cab
    sleep 0.5
  end
end


def bass_arps (n)
  play_pattern_timed [n, n, :r, n, :r, n+12, n+22, n+24], 0.25 if arps_on
end

def chords_chop (c)
  patt = "xx-x--x---x--x--".ring
  
  # consider long flang with volume chops instead of plays
  16.times do
    use_synth_defaults sustain: 0.2, release: 0.2, cutoff: 100, amp: 1.5
    play_chord c if patt.tick == "x" if chords_on
    wait 0.24999
  end
end


live_loop :chords, sync: "/live_loop/kick" do
  stop if bridge
  3.times do
    chords_chop (chord :F3, :m)
    chords_chop (chord :F3, :m7)
    chords_chop (chord_invert (chord :Bb2, :sus4),2)
    chords_chop (chord_invert (chord :Bb2, :maj),2)
  end
  sync "/live_loop/kick"
  1.times do
    chords_chop (chord_invert (chord :Db3, :maj),1)
    chords_chop (chord_invert (chord :Eb3, :maj),1)
    chords_chop (chord_invert (chord :Bb2, :sus4),2)
    chords_chop (chord_invert (chord :Bb2, :m),2)
  end
end


with_fx :flanger, phase: 16, phase_offset: 0.5, depth: 3.2 do
  
  ###################################################################################### BASS
  live_loop :bass, sync: "/live_loop/kick" do
    stop if bridge
    
    use_synth :tri
    use_synth_defaults amp: 2.5
    use_transpose -12
    ##| play :F2, sustain: 16, amp: 0.2
    if boys_in_love
      play_pattern_timed [:F2, :Ab2, :Bb2, :G2], [4,4,4,3.999] if bass_on
    elsif bridge
      3.times do
        play :F2; wait 2
      end
      wait 1.999
    else
      3.times do
        play_pattern_timed [:F2, :Ab2, :Bb2], [4,4,7.999] if bass_on
      end
      sync "/live_loop/kick"
      ##| play :Db1, sustain: 16, amp: 0.2
      play_pattern_timed [:Db2, :Eb2, :Bb2], [4,4,7.999] if bass_on
    end
  end
  
end

with_fx :flanger, phase: 16, depth: 4 do
  
  live_loop :bass_arp, sync: "/live_loop/kick" do
    stop if bridge
    use_synth :dsaw
    use_synth_defaults sustain: 0.18, release: 0.2, amp: 1.2
    
    sync "/live_loop/kick" unless arps_on
    23.times do
      bass_arps :F1
    end
    stop if bridge
    bass_arps :C1
    2.times do
      bass_arps :Db1
    end
    2.times do
      bass_arps :Eb1
    end
    4.times do
      bass_arps :Bb1
    end
  end
  
  
  live_loop :bass_bridge, sync: "/live_loop/kick" do
    stop unless bridge
    
    3.times do
      use_synth :dsaw
      use_synth_defaults sustain: 0.18, release: 0.2, amp: 1.2
      play :F1
      use_synth :tri
      use_synth_defaults amp: 2.5
      play :F1
      wait 2
    end
    use_synth :dsaw
    use_synth_defaults sustain: 0.18, release: 0.2, amp: 1.2
    play_pattern_timed [:F2,:Ab2,:Db3,:C3,:F2,:Ab2,:C3,:Ab3],0.25
  end
  
  #################################################################################### VERSE
  live_loop :verse, sync: "/live_loop/kick" do
    stop unless verse_on
    stop if boys_in_love or bridge
    use_synth :saw
    wait 1
    play_pattern_timed [:F4, :Eb4, :C4, :C4, :Bb3, :Ab3, :Bb3], 1
    wait 9
    play_pattern_timed [:F4, :Eb4, :C4, :C4, :Bb3, :Ab3, :Bb3], 1
    play_pattern_timed [:Bb3, :C4, :Bb3, :C4, :Bb3], [1,0.5,1.5,0.5,0.5]
    wait 5
    play_pattern_timed [:F3, :F3, :F3, :C4, :Bb3, :Ab3, :Bb3], 1
    wait 7
    play_pattern_timed [:G3, :Ab3, :Ab3, :Ab3, :Ab3], [0.5, 1.5]
    play_pattern_timed [:Bb3, :Ab3, :G3, :F3, :F3], [1.5,0.5,1,0.5,1]
    wait 8
  end
  
  live_loop :chorus, sync: "/live_loop/kick" do
    stop unless boys_in_love
    play :F4; wait 1
    wait 6.5
    play_pattern_timed [:C4, :Db4, :F4, :Db4], 1
    play_pattern_timed [:C4, :Db4, :F4, :F4], 1
    play :F4; wait 0.5
  end
end


################################################################################### CHORUS

with_fx :flanger, phase: 16, depth: 8 do
  live_loop :boysinlove, sync: "/live_loop/kick" do
    stop unless boys_in_love
    use_synth :saw
    use_transpose 12
    play_pattern_timed [:C5, :Ab4, :G4, :Eb4, :F4],[2,2,2,2,4.5]
    play_pattern_timed [:F4, :Ab4, :F4, :C5], [1,1,1,0.5]
  end
end

live_loop :bridge_top, sync: "/live_loop/kick" do
  stop unless bridge
  ##| stop if boysinlove == 1
  3.times do
    wait 1
    play_pattern_timed [:F4,:F4], 0.5
  end
  wait 2
end
