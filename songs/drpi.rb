# https://gitlab.com/milohax/hax/-/snippets/2231650

use_bpm 130
# Change which Generation by using a different transpose ;-)
use_transpose +2

one_third=1.0/3.0
two_third=2.0/3.0
whole=1

# Parts of the base line, with onomatopoic names
# Each fn accepts a note that it will play at
# and the parts are strung together in tho bass thread
define :dadabeda do |n|
  3.times do
    play n
    sleep one_third
  end
  
  play n
  
  sleep two_third
  play n-12
  sleep one_third
end

define :deedeebodee do |n|
  2.times do
    play n
    sleep two_third
  end
  play n-2, sustain: one_third, release: 0.1
  sleep one_third
  play n-5, sustain: one_third, release: 0.1
  sleep one_third
end

define :deedeeboup do |n|
  2.times do
    play n
    sleep two_third
  end
  play n+1, sustain: one_third, release: 0.1
  sleep one_third
  play n+3, sustain: one_third, release: 0.1
  sleep one_third
end


#### The bass line drives the melody for the whole theme
##   So this runs it in its own thread, and cues different
##   sections from here, at the right times.
##
##   The sections each run in their own live_loops and
##   sync from the cues.

in_thread(name: :bass) do
  with_fx :gverb, amp: 0.8 do
    with_fx :echo, phase: one_third,amp: 0.6 do
      
      use_synth :square
      use_synth_defaults decay: 0.4, decay_level: 0.4, sustain_level: 0.4
      
      #intro
      3.times do
        dadabeda :D2
      end
      deedeebodee :F2
      
      #lead-in
      3.times do
        dadabeda :D2
      end
      cue :leadin
      deedeebodee :F2
      
      #versey bit
      verses=2
      verse =0
      
      verses.times do
        verse = verse + 1
        2.times do
          dadabeda :D2
        end
        2.times do
          dadabeda :A1
        end
        2.times do
          dadabeda :D2
        end
        dadabeda :A1
        deedeeboup :A1
        2.times do
          dadabeda :D2
        end
        dadabeda :F2
        deedeeboup :A1
        2.times do
          dadabeda :D2
        end
        3.times do
          dadabeda :A1
        end
        deedeebodee :C2
        3.times do
          dadabeda :A1
        end
        if verse!=verses
          cue :leadin
          deedeebodee :F2
        else
          deedeeboup :A1
        end
      end
      
      ### chorusy bit
      cue :chorus
      4.times do
        dadabeda :F2
      end
      dadabeda :C2
      3.times do
        dadabeda :F2
      end
      dadabeda :Bb1
      
      dadabeda :F2
      dadabeda :Bb1
      dadabeda :F2
      2.times do
        dadabeda :A1
      end
      2.times do
        dadabeda :D1
      end
      
      ##outro
      3.times do
        dadabeda :A1
      end
      deedeebodee :C2
      3.times do
        dadabeda :A1
      end
      cue :outro
      deedeebodee :F2
      
      ##ending
      2.times do
        2.times do
          dadabeda :D2
        end
        dadabeda :A1
        cue :outro
        dadabeda :A1
      end
      
      use_synth :tb303
      sn = play :D1, release: 12, cutoff: 130, cutoff_slide: 2
      12.times do |c|
        control sn, cutoff: 120-(c*10)
        sleep 0.75
      end
      stop
    end
  end
end



#### the lead

# Play a note n sustained for s beats
define :playsust do |n, s|
  sn = play n, sustain: s, note_slide: 1.0/3.0
  sleep s
  return sn
end

# Slide to the next note and sustane
define :playslide do |sn, n, s|
  control sn, note: n, sustaine: s
  sleep s
end

### melody
live_loop :thelead do
  sync :leadin
  with_fx :flanger do
    with_fx :echo, phase: 1 do
      
      use_synth :sine
      
      s = playsust :A4, 1
      playslide s,:Bb5, 1
      
      playsust :A5, 6
      
      playsust :C6, 1+one_third
      playsust :A5, one_third
      playsust :G5, one_third
      
      playsust :A5, 7
      playsust :r, 1
      
      playsust :A5, 1
      playsust :F5, 1
      s = playsust :A4, 2
      
      playslide s, :C5, 1+two_third
      playsust :Bb4, one_third
      
      playsust :A4, 3
      
      playsust :G4, 1
      
      playsust :A4, 1+two_third
      playsust :Bb4, one_third
      
      s = playsust :A4, 1+two_third
      playslide s, :Bb5, one_third
      
      playsust :A5, 6
    end
  end
end

live_loop :thechorus do
  sync :chorus
  with_fx :ixi_techno, phase: 4.5, cutoff_min: 70 do
    use_synth :square
    playsust :A5, 6
    playsust :F5, 1
    playsust :A5, 1
    
    playsust :G5,1+one_third
    playsust :F5, one_third
    playsust :E5, one_third
    
    playsust :F5, 4
    
    playsust :F4, 1
    playsust :C6, 1
    playsust :D6, 1+one_third
    playsust :C6, one_third
    playsust :Bb5, one_third
    
    playsust :C6, 1
    playsust :F5, 1
    
    playsust :D6, 1+one_third
    playsust :C6, one_third
    playsust :Bb5, one_third
    
    playsust :C6, 1
    playsust :F5, two_third
    playsust :A5, one_third
    playsust :G5, 3+one_third
    playsust :F5, one_third
    playsust :E5, one_third
    
    playsust :F5, 4
  end
end

live_loop :ending do
  sync :outro
  with_fx :flanger do
    with_fx :echo, phase: 1 do
      use_synth :square
      s = playsust :A4, 1
      playslide s, :Bb5, 1
      playsust :A5, 4
    end
  end
end

##| live_loop :hissing do
##|  #TODO find a synth/ setting that can phase hiss like the TV synth
##|   use_synth :dark_ambience
##|   play :d5
##|   sleep 2
##| end

