#### Sonic Pi  -*- mode:ruby -*-

# Bach Preludes & Fugues 1-12 Well Tempered Clavier
# Book 1, Prelude 1.  Transcribed from
# https://www.youtube.com/watch?v=2oFjk0HynY4
# https://gitlab.com/milohax/hax/-/snippets/2231651
# https://gist.github.com/sinewalker/2739ceccc4dee7bac2613f9b94850882

define :bach do |n|
  #play an array of notes twice, with the last 3 repeated each time
  #this is the pattern used repeatedly in Prelude no.1
  2.times do
    play n[0], sustain: 8; sleep 1
    play n[1], sustain: 7; sleep 1
    if $style == :normal then
      2.times do
        play_pattern n[-3,3]
      end
    else  # OR... pick randomly ... because Computers!
      6.times do
        play choose(n[-3,3]); sleep 1
      end
    end
  end
end


# Using 16:1 for beats, since I don't know an easier way to specify 16ths
# and it's all even multiples anyway.  So that means the BPM is 16 times as fast
use_bpm 280
# It's sometimes nice to hear in keys other than C-Major.
use_transpose 0 # plus don't need to n.map! in bach function...

use_synth :chiplead
use_synth_defaults sustain: 0.5, release: 2

use_random_seed 4
$style = :pickle

####


bach [ :C4,  :E4,    :G4,  :C5,  :E5]
bach [ :C4,  :D4,    :A4,  :D5,  :F5]
#3
bach [ :B3,  :D4,    :G4,  :D5,  :F5]
bach [ :C4,  :E4,    :G4,  :C5,  :E5]
bach [ :C4,  :E4,    :A4,  :E5,  :A5]
#6
bach [ :C4,  :D4,   :Fs4,  :A4,  :D5]
bach [ :B3,  :D4,    :G4,  :D5,  :G5]
bach [ :B3,  :C4,    :E4,  :G4,  :C5]
#9
bach [ :A3,  :C4,    :E4,  :G4,  :C5]
bach [ :D3,  :A3,    :D4, :Fs4,  :C5]
bach [ :G3,  :B3,    :D4,  :G4,  :B4]
#12
bach [ :G3, :Bb3,    :E4,  :G4, :Cs5]
bach [ :F3,  :A3,    :D4,  :A4,  :D5]
bach [ :F3, :Ab3,    :D4,  :F4,  :B4]
#15
bach [ :E3,  :G3,    :C4,  :G4,  :C5]
bach [ :E3,  :F3,    :A3,  :C4,  :F4]
bach [ :D3,  :F3,    :A3,  :C4,  :F4]
#18
bach [ :G2,  :D3,    :G3,  :B3,  :F4]
bach [ :C3,  :E3,    :G3,  :C4,  :E4]
bach [ :C3,  :G3,   :Bb3,  :C4,  :E4]
#21
bach [ :F2,  :F3,    :A3,  :C4,  :E4]
bach [:Fs2,  :C3,    :A3,  :C4, :Eb4]
bach [:Ab2,  :F3,    :B3,  :C4,  :D4]
#24
bach [ :G2,  :F3,    :G3,  :B3,  :D4]
bach [ :G2,  :E3,    :G3,  :C4,  :E4]
bach [ :G2,  :D3,    :G3,  :C4,  :F4]
#27
bach [ :G2,  :D3,    :G3,  :B3,  :F4]
bach [ :G2, :Eb3,    :A3,  :C4, :Fs4]
bach [ :G2,  :E3,    :G3,  :C4,  :G4]
#30
bach [ :G2,  :D3,    :G3,  :C4,  :F4]
bach [ :G2,  :D3,    :G3,  :B3,  :F4]
bach [ :C2,  :C3,    :G3, :Bb3,  :E4]
#33
play :C2, sustain: 12, release: 4; sleep 1
play :C3, sustain: 11, release: 4; sleep 1
play_pattern [ :F3, :A3,    :C4, :F4, :C4, :A3,
               :C4, :A3, :F3, :A3,   :F3, :D3, :F3, :D3]
play :C2, sustain: 12, release: 4; sleep 1
play :B2, sustain: 11, release: 4; sleep 1
play_pattern [ :G4, :B4,    :D5, :F5, :D5, :B4,
               :D5, :B4, :G4, :B4,   :D4, :F4, :E4, :D4]
play_chord [:C2, :C3, :E4, :G4, :C5], amp: 4, sustain: 16, release: 8

