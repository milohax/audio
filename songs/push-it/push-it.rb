# Push It

# TODO
#
# This song is almost there, there is a bass-guitar part missing,
# the synths need tweeking, and also vocal samples,
# and a master cue string to control parts

# Counting in quavers (half-notes), so double the BPM
use_bpm 242

# directs parts by listing which to play.
#   b=bass, l=lead, p=push_it
# (could use symbols, but these are easier to type)
CONDUCTOR=''

live_loop :met do
  wait 1
end

live_loop :bar, sync: :met do
  wait 8
end

define :pattern do |pattern|
  return pattern.ring.tick == "x"
end

live_loop :bdrums, sync: :met do
  sample :drum_heavy_kick if pattern "x---xx--xx-x-x--"
  wait 1
end

live_loop :sdrums, sync: :met do
  sample :drum_snare_hard if pattern "--x---x---x---x-"
  wait 1
end

live_loop :cowbell, sync: :met do
  sample :drum_cowbell, rate: 0.75, amp: 0.5 if pattern "------xxx-x-xxx-----------------"
  wait 1.0/2
end

live_loop :shakers, sync: :met do
  sample :drum_cymbal_closed
  wait 1
end


live_loop :bass, sync: :bar do
  stop unless CONDUCTOR.include? 'b'
  use_synth :prophet
  use_transpose -12
  play_pattern_timed [:A3, :E3, :A3, :E3, :G3],
    [2,1,2,2,2]
  wait 7
  play_pattern_timed [:A3, :E3, :A3, :E3, :G3, :E3, :G3, :E3, :B3, :C4, :B3],
    [2,1,2,2,2,1,1,1,1,1,2]
  
end

live_loop :lead, sync: :bar do
  stop unless CONDUCTOR.include? 'l'
  use_synth :supersaw
  play_pattern_timed [:A4, :E5, :D5, :C5, :B4, :G3, :r, :G4, :B4, :C5, :B4, :G4],
    [2,1,2,2,2,1,1,1,1,1,1,1]
  play_pattern_timed [:A4, :E5, :D5, :C5, :B4, :G4, :F4, :G4, :B4, :C5, :B4, :G4],
    [2,1,2,2,2,1,1,1,1,1,1,1]
  
end


live_loop :push_it, sync: :bar do
  stop unless CONDUCTOR.include? 'p'
  use_transpose -0.75
  use_synth :organ_tonewheel
  use_synth_defaults amp: 2, sustain: 1#
  play_pattern_timed [:A4, :r, :Gs4, :A4, :Gs4, :A4, :r, :Gs4, :r, :Gs4, :A4, :Gs4, :A4],
    [1, 2, 1,1,1,1, 2, 1, 1, 1, 1, 1, 2]
end
