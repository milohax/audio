#I wanted to hear if Tainted Love and Dr. Who go together...  ;-)
# https://gitlab.com/milohax/hax/-/snippets/2231642
# https://gist.github.com/sinewalker/1a7fbb2126956085198d50e8cea628ad

use_bpm 120
live_loop :metro do
  tick
  cue :beet
  sleep 1
end

live_loop :bass, sync_bpm: :beet do
  ##| stop
  use_synth :subpulse
  use_sample_defaults sustain: 2/3.0
  play_pattern_timed [:E2,:E2,:G2,:G2,:C3,:C3],1
  play :G2; wait 2/3.0
  play :A2, sustain: 1; wait 1 + 1/3.0
end

live_loop :beeps, sync_bpm: :beet do
  ##| stop
  use_synth :prophet
  2.times do
    play :E3; wait 1
  end
  wait 3 + 2/3.0
  use_synth :square
  play :E5; wait 1 + 1/3.0
  play :E5; wait 1
end

# Play a note n sustained for s beats
define :playsust do |n, s|
  sn = play n, sustain: s, note_slide: 1.0/3.0
  sleep s
  return sn
end

# Slide to the next note and sustane
define :playslide do |sn, n, s|
  control sn, note: n, sustaine: s
  sleep s
end

### melody (dr.who - from an earlier gist: https://gist.github.com/sinewalker/7e4b8f226e985ca7f84892705564837d )
live_loop :thelead, sync_bpm: :beet do
  one_third= 1/3.0
  two_third= 2/3.0
  use_transpose +2
  
  ##| sync :leadin
  with_fx :flanger do
    with_fx :echo, phase: 1,amp: 0.8, decay: 1 do
      
      use_synth :sine
      
      ##| s = playsust :A4, 1
      ##| playslide s,:Bb5, 1
      
      playsust :A5, 6
      
      playsust :C6, 1+one_third
      playsust :A5, one_third
      playsust :G5, one_third
      
      playsust :A5, 7
      playsust :r, 1
      
      playsust :A5, 1
      playsust :F5, 1
      s = playsust :A4, 2
      
      playslide s, :C5, 1+two_third
      playsust :Bb4, one_third
      
      playsust :A4, 3
      
      playsust :G4, 1
      
      playsust :A4, 1+two_third
      playsust :Bb4, one_third
      
      s = playsust :A4, 1+two_third
      playslide s, :Bb5, one_third
      
      playsust :A5, 2
    end
  end
end

