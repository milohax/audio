# Support Week In Review

use_bpm 120

########## master parts control

# could use bools true/false, but 0/1 is faster to type
support = 1
tennor = 0

bass = 1
chords = 1
arps = 0

pips = 0

drums = 0
shakers = 1
ride = 1


################


live_loop :met0 do
  wait 1
end


with_fx :reverb, room: 0.8, mix: 0.4 do
  with_fx :flanger, phase: 6, wave: 3, mix: 1, depth: 14,
  amp: 1.5, stereo_invert_wave: 1, feedback: 0.2  do
    
    live_loop :pips, sync: :met0 do
      stop unless pips != 0
      play :Fs6, sustain: 0.1, release: 0.1, amp: 0.3
      wait 1
    end
    
    live_loop :chords, sync: :met0 do
      stop unless chords != 0
      
      use_synth :blade
      play_chord (chord :E4, '6'), sustain: 3                # E6 (E major 6th)
      wait 3.5
      play_chord (chord :Fs4, '7', invert: -1), sustain: 3   # F#7/E (F# dominant 7 with E in bass)
      wait 4.5
      play_chord (chord :D4, :add9, invert: -1), sustain: 3  # Dadd9 (D major add 9, inverted)
      wait 3.5
      play_chord (chord :E4, :add9, invert: -1), sustain: 3  # Eadd9 (E major add 9, inverted)
      wait 4.5
    end
    
    
    live_loop :bass, sync: :met0 do
      use_synth :fm
      stop unless bass != 0
      
      # Use modulo of odd/even beat tick, to alternate between first note,
      # since both are funky!
      first_bass_note = tick % 2 == 0 ? :Fs2 : :E2
      
      use_synth_defaults amp: 0.7
      
      play first_bass_note, sustain: 3
      wait 3.5
      
      play :Fs2, sustain: 4
      wait 4.5
      
      play :D2, sustain: 3
      wait 3.5
      
      play :E2, sustain: 4
      wait 4.5
    end
    
  end
end


with_fx :echo, phase: 0.75 do
  live_loop :arp, sync: :met0 do
    # Uncomment the line below to stop this loop
    stop unless arps != 0
    
    use_synth :dtri
    use_synth_defaults attack: 0, release: 0.3, amp: 0.8
    
    # Define arpeggio notes based on current chord
    current_beat = tick % 16  # Create a 16-step pattern
    
    notes = case current_beat / 4  # Integer division to get current chord
    when 0  # First chord (E6)
      chord(:E4, '6').shuffle
    when 1  # Second chord (F#7/E)
      chord(:Fs4, '7').shuffle
    when 2  # Third chord (Dadd9)
      chord(:D4, :add9).shuffle
    when 3  # Fourth chord (Eadd9)
      chord(:E4, :add9).shuffle
    end
    
    # Play one note from the chord
    play notes[current_beat % notes.length]
    
    # Add some rhythmic variation
    wait [0.25, 0.5, 0.25].choose
  end
end


use_random_seed 500

# Define scales once
e_scale = scale(:E4, :major_pentatonic)
fs_scale = scale(:Fs4, :major_pentatonic)
d_scale = scale(:D4, :major_pentatonic)

# Define rhythmic patterns
rhythms_3_5 = [
  [1, 1, 1, 0.5],               # Regular pattern
  [0.5, 0.5, 1, 1, 0.5],        # Faster start
  [1.5, 0.5, 1, 0.5],           # Syncopated
  [0.75, 0.75, 0.5, 1, 0.5],    # Triplet feel
  [2, 1, 0.5]                   # Held note
]

rhythms_4_5 = [
  [1, 1, 1, 1.5],               # Regular pattern
  [0.5, 0.5, 1, 1, 1.5],        # Faster start
  [1.5, 1, 1, 1],               # Syncopated
  [0.75, 0.75, 1.5, 1.5],       # Triplet feel
  [2, 1.5, 1]                   # Held note
]

# Define riffs
e_riffs = {
  4 => [
    [e_scale[0], e_scale[1], e_scale[2], e_scale[4]],
    [e_scale[4], e_scale[3], e_scale[2], e_scale[0]]
  ],
  5 => [
    [e_scale[0], e_scale[1], e_scale[2], e_scale[3], e_scale[4]],
    [e_scale[0], e_scale[2], e_scale[1], e_scale[3], e_scale[4]]
  ],
  3 => [
    [e_scale[2], e_scale[4], e_scale[3]],
    [e_scale[0], e_scale[2], e_scale[4]]
  ]
}

fs_riffs = {
  4 => [
    [fs_scale[0], fs_scale[1], fs_scale[2], fs_scale[3]],
    [fs_scale[4], fs_scale[3], fs_scale[1], fs_scale[0]]
  ],
  5 => [
    [fs_scale[0], fs_scale[1], fs_scale[2], fs_scale[3], fs_scale[4]],
    [fs_scale[4], fs_scale[3], fs_scale[2], fs_scale[1], fs_scale[0]]
  ],
  3 => [
    [fs_scale[0], fs_scale[2], fs_scale[4]],
    [fs_scale[3], fs_scale[1], fs_scale[0]]
  ]
}

d_riffs = {
  4 => [
    [d_scale[0], d_scale[2], d_scale[1], d_scale[3]],
    [d_scale[4], d_scale[3], d_scale[2], d_scale[0]]
  ],
  5 => [
    [d_scale[0], d_scale[1], d_scale[2], d_scale[3], d_scale[4]],
    [d_scale[3], d_scale[2], d_scale[1], d_scale[0], d_scale[1]]
  ],
  3 => [
    [d_scale[0], d_scale[2], d_scale[4]],
    [d_scale[3], d_scale[1], d_scale[0]]
  ]
}

# Function to pick and play a riff with matching rhythm
define :play_jazz_riff do |scale_riffs, duration|
  if duration == 3.5
    rhythm = rhythms_3_5.choose
  else
    rhythm = rhythms_4_5.choose
  end
  
  note_count = rhythm.length
  riff = scale_riffs[note_count].choose
  
  play_pattern_timed riff, rhythm
end

# Main tenor walk loop
live_loop :tenor_walk, sync: :met0 do
  stop if support != 0 || tennor == 0
  use_synth :pulse
  use_synth_defaults amp: 0.5, sustain: 0.3, release: 0.3
  
  # E6 section
  play_jazz_riff(e_riffs, 3.5)
  
  # F#7 section
  play_jazz_riff(fs_riffs, 4.5)
  
  # Dadd9 section
  play_jazz_riff(d_riffs, 3.5)
  
  # Eadd9 section (reusing E scale)
  play_jazz_riff(e_riffs, 4.5)
end


live_loop :what_to_do, sync: :met0 do
  stop unless support != 0
  use_synth :tri
  
  use_synth :supersaw
  use_synth_defaults sustain: 0.3
  
  play_pattern_timed [:Fs5, :Fs5, :Fs5, :Fs5,            # all my week I
                      :Fs5, :Cs5, :Cs5, :Cs5, :As4, :Ds5 # did not know what to do
  ],
    [0.5,1,1,1.5,
     0.5,0.5,0.5,0.25, 0.25,1.5]
    play_pattern_timed [:As4, :B4, :B4, :B4, :B4,  # but now it'ts time for
                        :B4, :B4, :B4, :As4, :Gs4, # the support week in
                        :Fs4, :Fs4],               # rev...
    [0.5,0.5,1,1,0.5,
     0.5,0.5,1,1,1,
     0.5, 0.5]
    end


####################### drums

with_fx :reverb, room: 1 do
  # Basic drum loop with some variation
  live_loop :drums, sync: :met0 do
    stop unless drums != 0
    # Kick pattern
    sample :bd_tek, amp: 1.0
    
    # Optional ghost kick
    sample :bd_tek, amp: 0.6 if one_in(3)
    
    # Hi-hat pattern
    wait 0.5
    sample :drum_cymbal_closed, amp: 0.5, rate: 1.2
    
    # Snare on beat 2
    wait 0.5
    sample :sn_dolf, amp: 0.8
    
    # Hi-hat again
    wait 0.5
    sample :drum_cymbal_closed, amp: 0.5, rate: 1.1
    
    # Ghost notes and variation
    wait 0.25
    sample :drum_cymbal_closed, amp: 0.3, rate: 1.3 if one_in(2)
    wait 0.25
    
    # Kick on the "and" of 3 sometimes
    sample :bd_tek, amp: 0.7 if one_in(4)
    
    # Hi-hat again
    wait 0.5
    sample :drum_cymbal_closed, amp: 0.5, rate: 1.2
    
    # Snare on beat 4
    wait 0.5
    sample :sn_dolf, amp: 0.8
    
    # Final hi-hat
    wait 0.5
    sample :drum_cymbal_closed, amp: 0.5, rate: 1.1
    
    # Fill at the end of measures
    if tick % 4 == 3  # Every 4th time through the loop
      sample :drum_cymbal_open, amp: 0.4, sustain: 0.1
      wait 0.25
      sample :drum_snare_hard, amp: 0.6
      wait 0.25
    else
      wait 0.5
    end
  end
  
  # Percussion elements
  live_loop :percussion, sync: :met0 do
    stop unless shakers != 0
    sleep 4  # Wait 4 beats
    
    # Add shaker or similar percussion
    with_fx :echo, phase: 0.5, decay: 2, mix: 0.3 do
      sample :perc_snap, amp: 0.4, rate: 0.9
    end
    
    sleep 4
  end
  
  # Ride cymbal pattern
  live_loop :ride, sync: :met0 do
    use_synth_defaults amp: 0.3, attack: 0.01, sustain: 0.1, release: 0.3
    stop unless ride != 0
    
    # Swing pattern on ride
    sample :drum_cymbal_soft, amp: 0.25
    sleep 0.66
    sample :drum_cymbal_soft, amp: 0.15
    sleep 0.34
    
    sample :drum_cymbal_soft, amp: 0.2
    sleep 0.66
    sample :drum_cymbal_soft, amp: 0.15
    sleep 0.34
    
    # Repeat with slight variation
    sample :drum_cymbal_soft, amp: 0.25
    sleep 0.66
    sample :drum_cymbal_soft, amp: 0.15
    sleep 0.34
    
    # Accent with bell
    sample :drum_cymbal_soft, amp: 0.3, rate: 1.1
    sleep 1
  end
end

