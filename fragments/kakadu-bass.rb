# This is a bass line from an Ed Wilson Big Band piece
# I have an old cassette tape "Up The Pace", but I don't have a working player
# so, this is from memory. Not sure which piece, but I think it's "Kakadu".
# The music features a great tenor sax solo (hence key of Eb), but this code
# only has the bass part and a simple swinging drum.

# Note that I've used Sonic Pi's knowledge of scales here to help work out
# what the notes are. Rather than list the note names, I used the scale and
# offset within the scale, and the timing is specified in 3rds of a beat.
# That's mighty confusing, especially because the bass actually ends with the
# note below the main scale, so have to add 7 to each offset appart from the
# the last note, but it's how I could remember the tune.

use_bpm 110


live_loop :met do
  wait 1
end

live_loop :bass do
  use_synth :beep
  use_synth_defaults sustain: 2/3.0, release: 2/3.0
  scale_a = scale :Eb1, :dorian, num_octaves: 2
  notes_a = ring 7+0,7+4, 7+7,7+6, 7+3,7+4, 7+2,7+3, 6
  timing = ring 2/3.0, 1/3.0, 2/3.0, 7/3.0, 2/3.0, 4/3.0, 2/3.0, 1, 1/3.0
  
  scale_b = scale :Db1, :minor, num_octaves: 2
  bridge = ring 6, 5, 7+3, 7+2
  
  
  t=0
  use_transpose 0
  4.times do
    notes_a.length.times do
      play scale_a[notes_a[t]]
      wait timing[t]
      t += 1
    end
  end
  use_transpose -2
  2.times do
    notes_a.length.times do
      play scale_a[notes_a[t]]
      wait timing[t]
      t += 1
    end
  end
  use_transpose 0
  t=0
  4.times do
    play scale_b[bridge[t]]
    wait 1.9
    t+=1
    sync :met
  end
end

live_loop :beet do
  stop
  play :Eb8
  sync :met
end

live_loop :rides do
  with_fx :reverb, room: 0.7 do
    sample :drum_cymbal_open, sustain: 0.5, decay: 0.13, amp: 0.4
    wait 1
    sample :drum_cymbal_pedal
    wait 2/3.0
    sample :drum_cymbal_closed
    wait 0.2
    sync :met
  end
end

live_loop :drums do
  with_fx :reverb, room: 0.7 do
    use_sample_defaults amp: 2.5
    sample :drum_bass_soft
    wait 1
    sample :drum_bass_soft
    wait 1
    sample :drum_snare_soft
    wait 3
    sample :drum_bass_soft
    wait 1+(2/3.0)
    sample :drum_bass_soft
    wait 0.9
    sync :met
  end
end

use_random_seed 4321

live_loop :tune do
  stop
  use_synth :chiplead
  scale_a = scale :Eb3, :dorian, num_octaves: 2
  scale_b = scale :Db3, :minor, num_octaves: 2
  4.times do
    8.times do
      with_swing 2/3.0, 3 do
        play scale_a.choose
        wait 1
      end
    end
  end
  play :Eb8
  use_transpose -2
  2.times do
    8.times do
      with_swing 1/3.0, 2 do
        play scale_a.choose
        wait 1
      end
    end
  end
  use_transpose 0
  8.times do
    with_swing 2/3.0, 2 do
      play scale_b.choose
      wait 1
    end
  end
end
