# https://gitlab.com/milohax/hax/-/snippets/2224037

live_loop :met1 do
  wait 1
end

live_loop :dr, sync: :met1 do
  sample :bd_ada
  wait 1
end

# here's the magic:
# Step through the supplied pattern string on each tick,
# and then return whether the current char is an 'x'.
# Use the ring method to loop steps the pattern forever. 
define :pattern do |pattern|
  return pattern.ring.tick == "x"
end

# This can then be used to step through a pattern 
# and do things if the "x" is in time for the current tick 
# like so:
live_loop :bosanova, sync: :met1 do
  ##| stop
  play :E4 if pattern "x--x--x---x--x--"
  wait 0.25
end
