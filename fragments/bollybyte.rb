# https://gitlab.com/milohax/hax/-/snippets/2231639
# https://gist.github.com/sinewalker/91ed9a3113c0e2c22cf23c5a45a72e07

live_loop :india do
  use_random_seed 4000
  16.times do
    sample "tabla_", choose
    sleep 0.125
  end
  sample "tabla_", choose
  sleep 0.125
  2.times do
    sample "tabla_", choose
    sleep 0.125/2
  end
  5.times do
    sample "tabla_", choose
    sleep 0.125
  end
  2.times do
    sample "tabla_", choose
    sleep 0.125/2
  end
  8.times do
    sample "tabla_", choose
    sleep 0.125
  end
end

