# Welcome to Sonic Pi

use_bpm 488
use_synth :tech_saws

live_loop :met do
  16.times do
    play :A2
    wait 1
  end
end

live_loop :fizz_buzz do
  sync :met
  i = 1
  128.times do
    if factor?(i, 15)
      play :G3
    elsif factor?(i,5)
      play :D4
    elsif factor?(i,3)
      play :C4
    else
      play :A3
    end
    wait 1
    i += 1
  end
end

live_loop :drums do
  ##| stop
  sync :met
  
  7.times do
    sample :drum_bass_soft
    wait 2
    sample :drum_snare_soft
    wait 2
  end
  sample :drum_heavy_kick
  wait 2
  sample :drum_snare_hard
  wait 1
  sample :drum_snare_hard
end
