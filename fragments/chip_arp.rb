# https://gitlab.com/milohax/hax/-/snippets/2231640
# https://gist.github.com/sinewalker/1374e40a8778cce8538db09ab1ee7811

define :chiparp do |notes, duration, c_amp=0.8, c_Hz=30, c_sust_ratio=1, c_step=1|
  tx=bt(1) # seconds for 1 beat
  in_thread do
    use_bpm 60 # normalise bpm to 1 beat per second
    c_speed = 1.0/c_Hz #the chip speed is better converted from Hz
    count = (duration * tx / c_speed)
    
    use_synth :chiplead
    use_synth_defaults amp: c_amp, sustain: c_speed*c_sust_ratio,
      attack: 0, decay: 0, release: 0
    puts "chip arp!"; puts notes; puts duration; puts count; puts tx
    n = 0
    with_debug false do
      count.times do
        play notes[n]
        wait c_speed
        n = n + c_step
      end
    end
  end
end

define :chiparp_wait do |notes, dur|
  # Use defaults
  ##| chiparp notes, dur
  # or override them (amp, speed, sust_ratio, direction)
  chiparp notes, dur, 1.6,25,0.8,-1
  wait dur
end

#####

# dinky tune - do better!
# but this demonstrates chiparp arpegio rate is independant of the global BPM, yet stretches to fit beats

use_bpm 80
use_synth :chiplead
play_pattern_timed [:Bb4, :D5], [1, 1]
4.times do
  chiparp_wait((chord :e5, :minor), 2)
  ##| chiparp_wait((chord :b4, :minor, invert: 1), 1)
  chiparp_wait((chord :a4, :minor), 1)
  chiparp_wait((chord :a4, :minor, invert: 1), 1)
end
