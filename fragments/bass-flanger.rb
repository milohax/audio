# Bass octave flanger
# experiment with a bass drone, dimly recollecting a didjerido?
live_loop :bass do
  with_fx :octaver do
    with_fx :flanger do
      play :C2, attack: 0.5, sustain: 2, release: 0.1
      play :C3, attack: 0.1, sustain: 2.5, release: 0.5
      wait 2
    end
  end
end
