use_bpm 240
use_debug false

##| puts scale(:A3,  :ionian)      # I   ("major")
##| puts scale(:B3,  :dorian)      # II
##| puts scale(:Cs4, :phrygian)    # III
##| puts scale(:D4,  :lydian)      # IV
##| puts scale(:E4,  :mixolydian)  # V
##| puts scale(:Fs4, :aeolian)     # VI  ("minor")
##| puts scale(:Gs4, :locrian)     # VII


##| play_pattern scale(:A3,  :ionian)
##| play_pattern scale(:B3,  :dorian)
##| play_pattern scale(:Cs4, :phrygian)
##| play_pattern scale(:D4,  :lydian)
##| play_pattern scale(:E4,  :mixolydian)
##| play_pattern scale(:Fs4, :aeolian)
##| play_pattern scale(:Gs4, :locrian)

puts scale(:A3, :aeolian)    # VI   ("minor")
puts scale(:B3, :locrian)    # VII
puts scale(:C4, :ionian)     # I    ("major")
puts scale(:D4, :dorian)     # II
puts scale(:E4, :phrygian)   # III
puts scale(:F4, :lydian)     # IV
puts scale(:G4, :mixolydian) # V


play_pattern scale(:A3, :aeolian)
play_pattern scale(:B3, :locrian)
play_pattern scale(:C4, :ionian)
play_pattern scale(:D4, :dorian)
play_pattern scale(:E4, :phrygian)
play_pattern scale(:F4, :lydian)
play_pattern scale(:G4, :mixolydian)

