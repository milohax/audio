#https://in-thread.sonic-pi.net/t/public-energy-three-o-three-with-sonic-pi/3548

use_bpm 134
t = 0.25  #general sleep value
x = 0.25  #general release value
lfo1 = ()
lfo2 = ()
lfo3 = ()

live_loop :lfo do
  lfo1 = (range 60,130,0.5).mirror.tick    #for cutoff
  lfo2 = (range -0.75,0.75,0.05).mirror.tick #for panning
  sleep 1
end

live_loop :resonance do
  lfo3 = (range 0.5,1,0.05).mirror.tick #for resonance
  sleep 2
end

define :t303 do |nte,rls,slp|  #note, release, sleep
  use_synth :tb303
  
  #TODO this is a little bit robotic, because the controls are applied on the beats.
  #     maybe we can slide the cutoff/res?
  play nte , release: rls, cutoff: lfo1, pan: lfo2, res: lfo3
  sleep slp
end
live_loop :riff, sync: "/live_loop/kick" do
  use_transpose -11
  X = x*6
  T = t*3
  notes = (ring :e3, :e3, :e4, :e3,  :e4, :e3, :e3, :g3,  :r, :g2, :g3, :g4, :a3, :b4, :d3, :e3)
  releases = (ring X,x,x,x)
  times = (ring t,t,t,t, t,t,t,t)
  
  notes.length.times do
    t303(notes.tick, releases.look, times.look)
  end
end

live_loop :kick do
  ##| stop
  sample :bd_haus, amp: 2
  sleep 1
  sample :sn_generic, amp: 0.5
  sample :bd_haus, amp: 2
  sleep 1
end
