# DRAFT
# I heard this is the first Matrix movie, unsure where and can't find it on Spotify.
# I think it's in the scene where Neo and the group enter the Matrix to find the Oracle, and Cypher leaves a phone to trace the group's location for the agents.

# It's an interesting 7/8 ascending line. Takes about 48 bars to cycle back into sync.

matrix = [:D4, :E4, :F4, :D4, :G4, :D4, :A4].ring

dur = 0.5
use_bpm 120

live_loop :metro do
  sleep dur
  set :position, tick
end

live_loop :au0 do
  ##| stop
  use_synth :tri
  play matrix[sync :position], pan: -0.5
end

live_loop :beat_chord do
  sync "/live_loop/metro"
  use_synth :fm
  play_chord chord(:d4, :minor), sustain: 3.5
  wait 3.8
end
