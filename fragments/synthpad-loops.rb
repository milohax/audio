# play with the ixi/slicer fx phases, and the ixi res, and start up the bass + simple drums
#
# I found that if you wait/sleep at the end of the drum/bass loops, you'll miss the cue for the start
# of the bar, so that's why the final waits are commented out.  Also if you sync at the start, it'll
# again miss the cue and wait a whole bar.  Not sure best ways to resolve this -- just comment where
# they sync, I suppose.
#
# https://gitlab.com/milohax/hax/-/snippets/2231644
#
use_bpm 120



# updated: phases longer than 4 require the chords be looped
# I tried putting the ixi_techno fx around the live_loop.  This works, but you can't then change the params and re-run:
# the child thread seems to inherit the fx and it's not being redefined because nothing within the :pads loop changes.
# so instead change the 2.times

live_loop :pads do
  use_synth :tb303
  use_synth_defaults sustain: 1.5
  with_fx :ixi_techno, phase: 8, res: 0.85,phase_offset: 0.5 do
    with_fx :slicer, phase: 1/4.0 do #0.000000000000001 do
      2.times do
        cue :bar
        play (chord_invert (chord :E3, :min), 0); wait 2;
        play (chord_invert (chord :C3, :maj), 1); wait 2;
        play (chord_invert (chord :A3, :min), -1); wait 2;
        play (chord_invert (chord :D3, :maj), 1); wait 2;
      end
    end
  end
end



live_loop :bass do
  stop
  sync :bar
  use_synth :mod_fm
  use_synth_defaults sustain: 2, mod_phase: 1, mod_phase_offset: 0.5, mod_range: 12, cutoff: 80
  play :E2; wait 2
  play :C2; wait 2
  play :A2; wait 2
  play :D2; #wait 1.999999999999999
  
end

live_loop :drums do
  stop
  sync :bar
  sample :bd_zome; wait 1
  sample :drum_snare_hard; wait 1
  sample :bd_zome; wait 0.25
  sample :bd_zome; wait 0.75
  sample :drum_snare_hard; wait 1
  
  sample :bd_zome; wait 1
  sample :drum_snare_hard; wait 1
  sample :bd_zome; wait 0.25
  sample :bd_zome; wait 0.75
  sample :drum_snare_hard;
  ##| sync :bar
end
