
use_bpm 90

live_loop :metro do
  wait 1
end


live_loop :tb, sync: "/live_loop/metro" do
  
  seq = [0,0,0,0, 12,0,0,0, 7,0,0,0, 0,1,0,0]
  
  with_synth :tb303 do
    play :e2 + seq.tick, cutoff: rand(100)+29, cutoff_min: rand(60),
      decay: 0.25, release: 0.02, pan: [-0.8,0,0.8].choose
    wait 0.25
  end
end

live_loop :dr do
  sample :drum_bass_hard
  wait 0.25
  sample :drum_bass_hard
  sync "/live_loop/metro"
  sample :sn_dub
  sync "/live_loop/metro"
end

live_loop :drone do
  sync "/live_loop/metro"
  with_synth :beep do
    play :e1, sustain: 4, amp: 2.5
    3.times do
      sync "/live_loop/metro"
    end
  end
end

