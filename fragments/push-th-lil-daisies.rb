# these are the verse chords from Ween - Push Th' Little Daisies
chords = [(chord :Bb3, '6'), (chord :F, :min), (chord :Eb, :maj), (chord :Gb, '6'),
          (chord :Bb3, '6'), (chord :F, :min), (chord :Eb, '6'), (chord :Gb, :maj), (chord :Ab, '6')].ring()

susts = [4,4,4,4,4,4,4,2,2].ring()

live_loop :guit do
  use_bpm 120
  i = tick
  play chords[i], sustain: susts[i] * 3/4
  wait susts[i]
end

# This might go well against a remix version of Command Keen
#https://www.youtube.com/watch?v=rYHKw1OuZUI
