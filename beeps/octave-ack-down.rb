dur = 0.12
pattern = [83,83,83,71,71,71]
use_synth_defaults sustain: dur, decay: 0, release: 0
play_pattern_timed pattern, dur
