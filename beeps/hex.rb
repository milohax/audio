# Hex (Discworld II: Missing Presumed ...!?)

https://gitlab.com/milohax/hax/-/snippets/2195975

use_bpm (9.5*60)
use_synth :square
use_random_seed 32767

upper_hz     = 512
upper_range  = 256
lower_hz     = 128
lower_range  = 96
woop_steps   = 12
beep_low_hz  = 1024
beep_high_hz = 4096

in_thread do
  use_synth_defaults sustain: woop_steps*2-1,
    attack: 0.5, decay: 0.5, release: 0.5, amp: 0.2

  with_fx :echo, phase: woop_steps / 3 do
    woop = play hz_to_midi(lower_hz)
    loop do
      target = hz_to_midi(upper_hz + rand(upper_range))
      control woop, note: target, slide: woop_steps
      wait woop_steps
      target = hz_to_midi(lower_hz - rand(lower_range))
      control woop, note: target, slide: woop_steps
      wait woop_steps
      woop = play target
    end
  end
end

live_loop :beeps0 do
  beeps_per_loop = 64
  beeper = play hz_to_midi(rrand(beep_low_hz,beep_high_hz)),
    sustain: beeps_per_loop, release: 0.5
  beeps_per_loop.times do
    control beeper, note: hz_to_midi(rrand(beep_low_hz,beep_high_hz)), slide: 0.07
    wait 1
  end
end

live_loop :beeps1, sync: "/live_loop/beeps0" do
  beeps_per_loop = 64
  offset_hz = 512
  beeper = play hz_to_midi(rrand(beep_low_hz,beep_high_hz) - offset_hz),
    sustain: beeps_per_loop, release: 0.5
  beeps_per_loop.times do
    control beeper, note: hz_to_midi(rrand(beep_low_hz,beep_high_hz) - offset_hz), slide: 0.07
    wait 1
  end
end

