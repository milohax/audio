# Powering up/changing alert sound. Starting note and speed are configurable
dur = 0.09
pattern = [0,12,24,25,26,27,28,29]
start_note = 54
use_synth_defaults sustain: dur, decay: 0, release: 0
pattern.each do |n|
  play start_note + n; wait dur
end
