# https://gitlab.com/milohax/hax/-/snippets/2195909
# Old-school TARDIS interior hum.  The weird wait frac (where `frac=1.0/0.2`) is to provide a non-zero wait in the loop, with minimal glitch. There must be a better way, maybe a sync?

puts midi_to_hz(:Ds3)
puts midi_to_hz(:Ds4)


live_loop :tardis_hum do
  low = hz_to_midi(155)
  high = hz_to_midi(300)
  frac=1.0/0.02
  play low, sustain: frac, release: 0.5, env_curve: 2, pan: -1
  play low, sustain: frac, release: 0.5, env_curve: 2, pan: -1
  
  play high, sustain: frac, release: 0.5, env_curve: 2, pan: 1
  wait frac
end

##| tardis0 = play hz_to_midi(156), sustain: 300, release: 0.5, env_curve: 2
##| tardis1 = play hz_to_midi(302), sustain: 300, release: 0.5, env_curve: 2

##| control tardis1, pitch: hz_to_midi(900)
