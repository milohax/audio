dur = 0.11
with_fx :bitcrusher,bits: 3 do
  use_synth_defaults sustain: dur, decay: 0, release: 0
  play_pattern_timed [83,85,87], dur
  play_pattern_timed [92,88], dur * 2, decay: 0.2
end
