Sonic Pi has an experimental, undocumented feature to support snippets, which autoexpand with the TAB key.

Here's the issue:  https://github.com/sonic-pi-net/sonic-pi/issues/587

Specifically, Sam's comment:  https://github.com/sonic-pi-net/sonic-pi/issues/587#issuecomment-131945899 

> ... [T]o play with it now (with the caveat that it all may drastically change) you just need to create a file with the contents of your snippet with an .sps extension in a directory somewhere on your machine. You might want to create a snippets directory for your own personal snippets. The name of the file doesn't matter. You then load all the snippets in a given directory with:
>
> `load_snippets("/path/to/snippets/dir")`
> 
> At the top of the file you'll need to put some metadata telling Sonic Pi when to trigger your snippet and where to leave the cursor.
>
> Here's an example of a complete snippet (from here: https://github.com/samaaron/sonic-pi/blob/master/etc/snippets/fx/fx_reverb.sps):
>
> ```ruby
> # key: fx r
> # point_line: 1
> # point_index: 2
> # --
> with_fx :reverb do
>
> end
> ```
>
> `# key:` tells Sonic Pi which letters to look for prior to point (the cursor) when tab is hit. <br/>
> `# point_line:` tells Sonic Pi which line to place the point on (relative to the snippet). This is 0 indexed <br/>
> `# point_index:` tells Sonic Pi which position on the line to place the po I int (also 0 indexed). This is also relative to the snippet.


Some example snippets:

 * Sam:  https://github.com/sonic-pi-net/sonic-pi/tree/main/etc/snippets
 * Hopbit: https://github.com/hopbit/sonic-pi-snippets
 * DarinWilson: https://gist.github.com/darinwilson/137764d4b06072de626c