# key: metro
# point_line: 0
# point_index: 9 
# --
use_bpm 

live_loop :met0 do
  wait 1
end

define :count do |beats|
  beats.times do
    sync :met0
  end
end
 